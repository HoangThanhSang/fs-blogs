import { useEffect, useState } from "react";
import axios from "axios";

function useFetchData(apiEndpoint: string) {
    const [allData, setAllData] = useState([]);
    const [loading, setLoading] = useState(true);
    const [initialLoad, setInitialLoad] = useState(true);

    useEffect(() => {
        if(initialLoad) {
            setInitialLoad(false);
            setLoading(false);
            return;
        }

        setLoading(true);


        const fetchAllData = async () => {
            try {
                const res = await axios.get(apiEndpoint);
                const data = res.data;
                setAllData(data);
                setLoading(false);
            } catch(e) {
                console.log('Error fetching data: ', e)
                setLoading(false);
            }
        }

        if(apiEndpoint) {
            fetchAllData();
        }

    }, [initialLoad, apiEndpoint]);
    
    return { allData, loading }
}

export default useFetchData;