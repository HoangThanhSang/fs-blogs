import React from 'react'
import { useSession, signIn, signOut } from "next-auth/react"
import { useRouter } from 'next/router';
import { CiFaceSmile } from "react-icons/ci";
import Loading from '@/components/Loading';

export default function Login() {
    const { data: session, status } = useSession();

    if(status == 'loading' ) {
        return <>
            <div className='loadingdata flex flex-col flex-center wh_100'>
                <Loading />
                <h1>Loading...</h1>
            </div>
        </>
    }
    
    const router = useRouter();

    async function login() {
        await router.push('/');
        await signIn();
    }

    if(session) {
        router.push('/')
    }

    if(!session) {
        return (
            <>
                <div className="loginfront flex flex-center flex-col full-w">
                    <CiFaceSmile width={250} height={250} />
                    <h1>Welcome Admin</h1>
                    <p>Dont need to try to be cool</p>

                    <button onClick={login} className='mt-2'>
                        Login with google
                    </button>
                </div>
            </>
        )
    }
}
