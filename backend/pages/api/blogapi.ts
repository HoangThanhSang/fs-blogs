import React from 'react'
import { mongooseconnect } from '@/lib/mongoose'
import { Blog } from '@/models/blog';

export default async function handle(req: any, res: any) {
  await mongooseconnect()

  const { method } = req;
  if(method == 'POST') {
    const { title, slug, description, blogcategory, tags, status } = req.body;
    const blogDoc = await Blog.create({
        title, slug, description, blogcategory, tags, status
    })
    res.json(blogDoc);
  }

  if(method == 'GET') {
    if(req?.params.id) {
        let blogs = await Blog.findById(req.params.id);
        res.json(blogs)
    } else {
        let blogs = await Blog.find();
        let reversesBlogs = blogs.reverse();
        res.json(reversesBlogs)
    }
  }

  if(method == 'PUT') {
    const { _id, title, slug, description, blogcategory, tags, status } = req.body;
    const blogDoc = await Blog.updateOne({_id}, {
        title, slug, description, blogcategory, tags, status
    })
    res.json(true);
  }

  if(method == 'GET') {
    if(req?.params.id) {
        await Blog.deleteOne({_id: req.params.id});
        res.json(true)
    }
  }

  if(method == 'DELETE') {
    if(req.query.id) {
        await Blog.deleteOne({_id: req.query.id});
        res.json(true)
    }
  }
}
