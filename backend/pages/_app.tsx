import Aside from "@/components/Aside";
import Header from "@/components/Header";
import "@/styles/globals.css";
import type { AppProps } from "next/app";
import { SessionProvider } from 'next-auth/react'
import { useState } from "react";
import Loading from "@/components/Loading";

export default function App({ Component, pageProps: {session, ...pageProps} }: AppProps) {

  const [loading, setLoading] = useState(false);
  return <>
    <SessionProvider
      session={session}
    >
      { loading ? (
        <div className="flex flex-column flex-center wh-100"> 
          <Loading />
          <h1>Loading...</h1>
        </div>
      ) : (
          <>
          <Header />
          <Aside />
          <main>
            <Component {...pageProps} />
          </main>
          </>
      ) }
      
    </SessionProvider>
    
  </>;
}
