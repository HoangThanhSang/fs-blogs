import axios from 'axios';
import { useRouter } from 'next/router';
import React, { useState } from 'react'
import MarkdownEditor from 'react-markdown-editor-lite'
import ReactMarkdown from 'react-markdown'

const Blog: React.FC<any> = ({
  _id,
  title: existingTitle,
  slug: existingSlug,
  blogcategory: existingBlogcategory,
  description: existingDescription,
  tags: existingTags,
  status: existingStatus
}) => {

  const [redirect, setRedirect] = useState(false);
  const router = useRouter();

  const [title, setTitle] = useState(existingTitle || '');
  const [slug, setSlug] = useState(existingSlug || '');
  const [blogcategory, setBlogcategory] = useState(existingBlogcategory || '');
  const [description, setDescription] = useState(existingDescription || '');
  const [tags, setTags] = useState(existingTags || '');
  const [status, setStatus] = useState(existingStatus || '');

  async function createProduct(ev: any) {
    ev.preventDefault();
    const data = { title, slug, description, blogcategory, tags, status };

    if(_id) {
      await axios.put('/api/blogapi', {...data, _id})
    } else {
      await axios.post('/api/blogapi', {...data, _id})
    }

    setRedirect(true);
  } 

  if(redirect) {
    router.push("/");
    return null;
  }

  return (
    <div>
        <form onSubmit={createProduct} className='addWebsiteform'>

            <div className='w-100 flex flex-col flex-left mb-2'>
                <label htmlFor='title'>Title</label>
                <input value={title} onChange={(e) => setTitle(e.target.value )} type='text' id='title' placeholder='Enter small title'/>
            </div>

            <div className='w-100 flex flex-col flex-left mb-2'>
                <label htmlFor='title'>Slug</label>
                <input value={slug} onChange={(e) => setSlug(e.target.value )} type='text' id='slug' placeholder='Enter small title' required/>
            </div>

            <div className='w-100 flex flex-col flex-left mb-2'>
                <label htmlFor='category'>Category</label>
                <select name='category' id='category'
                  value={blogcategory} onChange={(e) => setBlogcategory(Array.from(e.target.selectedOptions, option => option.value))}
                >
                  <option value='htmlcssjs'>
                    Html, Css & Javascript
                  </option>
                  <option value='nextjs'>
                   Next js
                  </option>
                  <option value='database'>
                    Database
                  </option>
                  <option value='deployment'>
                    Deployment
                  </option>
                </select>
                <p className="existingcategory flex gap-1 mt-1 mb-1">
                  Selected: <span>category</span>
                </p>
            </div>

            <div className='description w-100 flex flex-col flex-left mb-2'>
              <label htmlFor='description'>
                Blog content
              </label>

              <MarkdownEditor 
                value={description}
                onChange={(e) => setDescription(e.text)}
                style={{width: '100%', height: '400px'}}
                renderHTML={(text) => (
                  <ReactMarkdown components={{
                    code: ({node, inline, className, children, ...props}) => {
                      const match = /language-(\w+)/.exec(className || '');
                      if(inline) {
                        return <code>{children}</code>
                      } else if(match) {
                        return (
                          <div style={{ position: 'relative' }}>
                            <pre style={{ padding: '0', borderRadius: '5px', overflowX: 'auto', whiteSpace: 'pre-wrap' }} {...props}>
                              <code>{children}</code>
                            </pre>
                            <button style={{position: 'absolute', top: '0', right: '0', zIndex: '1'}}
                              onClick={() => navigator.clipboard.writeText(children)}
                            >Copy code</button>
                          </div>
                        )
                      }
                    }
                  }}>

                  </ReactMarkdown>
                )}
              />

            </div>

            <div className='w-100 flex flex-col flex-left mb-2'>
                <label htmlFor='tags'>Tags</label>
                <select name='tags' id='tags' multiple
                  value={tags} onChange={(e) => setTags(Array.from(e.target.selectedOptions, option => option.value))}
                >
                  <option value='html'>
                    Html
                  </option>
                  <option value='css'>
                   css
                  </option>
                  <option value='javascript'>
                    Javascript
                  </option>
                  <option value='deployment'>
                    Deployment
                  </option>
                  <option value='nextjs'>
                    Nextjs
                  </option>
                  <option value='reactjs'>
                    React js
                  </option>
                </select>
                <p className="existingcategory flex gap-1 mt-1 mb-1">
                  Selected: <span>tegs</span>
                </p>
            </div>

            <div className='w-100 flex flex-col flex-left mb-2'>
                <label htmlFor='status'>Status</label>
                <select name='status' id='status'
                  value={status} onChange={(e) => setStatus(e.target.value )}
                >
                  <option value='draft'>
                    Draft
                  </option>
                  <option value='publish'>
                    Publish
                  </option>
                </select>
                <p className="existingcategory flex gap-1 mt-1 mb-1">
                  Selected: <span>Status</span>
                </p>
            </div>

            <div className="w-100 mb-2">
              <button type='submit' className='w-100 addwebbtn flex-center'> SAVE BLOG</button>
            </div>
        </form>
    </div>
  )
}
