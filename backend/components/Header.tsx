import React, { useState } from 'react';
import { RiBarChartHorizontalLine } from "react-icons/ri";
import { GoScreenFull } from "react-icons/go";
import { IoIosNotificationsOutline } from "react-icons/io";
import { BiExitFullscreen } from "react-icons/bi";
import { FaUser } from "react-icons/fa";
import { useSession } from 'next-auth/react';

export default function Header() {
  const { data: session, status } = useSession();
  const [isFullscreen, setIsFullscreen] = useState<boolean>(false);

  const toggleFullscreen = () => {
    if(!document.fullscreenElement) {
      document.documentElement.requestFullscreen().then(() => {
        setIsFullscreen(true);
      })
    } else {
      if(document.exitFullscreen) {
        document.exitFullscreen().then(() => {
          setIsFullscreen(false)
        })
      }
    }
  }

  return (
    <>
      <header className='header flex flex-sb'>
        <div className='logo flex gap-2'>
          <h1>ADMIN</h1>
          <div className='headerham flex flex-center'>
            <RiBarChartHorizontalLine />
          </div>
        </div>

        <div className='rightnav flex gap-2'>
          <div onClick={toggleFullscreen}>
            { isFullscreen ? <BiExitFullscreen /> : <GoScreenFull /> }
          </div>
          <div className="notification">
            <IoIosNotificationsOutline />
          </div>
          <div className="profileNav">
            { session ? <img width={50} height={50} src={session.user!.image!}  alt='uses'/> : <FaUser /> }
          </div>
        </div>
      </header>
    </>
  );
}
