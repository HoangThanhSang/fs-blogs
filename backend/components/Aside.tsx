import Link from 'next/link'
import React, { useEffect, useState } from 'react'
import { IoHome } from "react-icons/io5";
import { BsFillPostcardFill } from "react-icons/bs";
import { MdOutlineAddPhotoAlternate } from "react-icons/md";
import { IoSettingsOutline } from "react-icons/io5";
import { MdOutlinePending } from "react-icons/md";
import { useRouter } from 'next/router';

export default function Aside() {

  const router = useRouter();
  const [clicked, setClicked] = useState<Boolean>(false);  
  const [activeLink, setActiveLink] = useState('/');

  const handleClick = () => {
    setClicked(!clicked);
  }

  const handleLinkClick = (link: string) => {
    setActiveLink(link);
    setClicked(false);
  }

  useEffect(() => {
    setActiveLink(router.pathname);
  }, [router.pathname])

  return (
    <>
        <aside className='asideleft'>
            <ul>
                <Link href="/">
                    <li className={ activeLink  == '/' ? 'navactive' : ''}
                        onClick={() => {handleLinkClick('/')}}
                    > 
                        <IoHome />
                        <span>Dashboard</span>
                    </li>
                </Link>
                <Link href="/blogs">
                    <li className={ activeLink  == '/blogs' ? 'navactive' : ''}
                        onClick={() => {handleLinkClick('/blogs')}}
                    > 
                        <BsFillPostcardFill />
                        <span>Blogs</span>
                    </li>
                </Link>
                <Link href="/blogs/addblog">
                    <li className={ activeLink  == '/blogs/addblog' ? 'navactive' : ''}
                        onClick={() => {handleLinkClick('/blogs/addblog')}}
                    > 
                        <MdOutlineAddPhotoAlternate />
                        <span>AddBlogs</span>
                    </li>
                </Link>
                <Link href="/draft">
                    <li className={ activeLink  == '/draft' ? 'navactive' : ''}
                        onClick={() => {handleLinkClick('/draft')}}
                    > 
                        <MdOutlinePending />
                        <span>Pending</span>
                    </li>
                </Link>
                <Link href="/settings">
                    <li className={ activeLink  == '/settings' ? 'navactive' : ''}
                        onClick={() => {handleLinkClick('/settings')}}
                    >
                        <IoSettingsOutline />
                        <span>Settings</span>
                    </li>
                </Link>
            </ul>
        </aside>
    </>
  )
}
